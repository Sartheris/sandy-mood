package com.sarth.sandymood.utils;

import com.sarth.sandymood.R;

public class MyConstants {

	public static final int MAX_IMAGES = 11;
	public static final int MUSIC = R.raw.desert;
	public static final String IMAGE_RESOURCE = "android.resource://" + "com.sarth.sandymood" + "/drawable/" + "desert";
	public static final String GOOGLE_PLAY_URL = "https://play.google.com/store/apps/details?id=com.sarth.sandymood";
	
	public static final String GOOGLE_PLAY_URL_MORE_APPS = "https://play.google.com/store/apps/developer?id=SARTH+INC";
	public static final String DEVELOPER_EMAIL = "sartheris@gmail.com";
}