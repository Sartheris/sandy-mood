package com.sarth.sandymood.utils;

import java.util.Random;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.sarth.sandymood.R;

public class MyUtils {

	public static void hideSystemUI(View decorView) {
		// Set the IMMERSIVE flag.
		// Set the content to appear under the system bars so that the content
		// doesn't resize when the system bars hide and show.
		decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
				| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
				| View.SYSTEM_UI_FLAG_IMMERSIVE);
	}

	// This snippet shows the system bars. It does this by removing all the flags
	// except for the ones that make the content appear under the system bars.
	public static void showSystemUI(View decorView) {
		decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
	}

	public static int getRandomImage(int currentImage) {
		// Get random number with excluding the last value
		int i;
		while (true) {
			i = new Random().nextInt(MyConstants.MAX_IMAGES);
			if (i != currentImage) {
				break;
			}
		}
		return i;
	}

	public static void sendReportEmail(Context context) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL, new String[] { MyConstants.DEVELOPER_EMAIL });
		i.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
		i.putExtra(Intent.EXTRA_TEXT, getDeviceInfo(context));
		try {
			context.startActivity(Intent.createChooser(i, "Send mail..."));
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(context, R.string.send_email_error, Toast.LENGTH_LONG).show();
		}
	}

	private static String getDeviceInfo(Context context) {
		String deviceBrand = Build.MANUFACTURER;
		String deviceModel = Build.MODEL;
		int androidVersion = Build.VERSION.SDK_INT;
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;
		String emailBody = deviceBrand + " " + deviceModel + ", OS Version: " + androidVersion 
				+ "\nDisplay: " + width + "x" + height;
		return emailBody;
	}
}