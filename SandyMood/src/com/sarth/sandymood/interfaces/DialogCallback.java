package com.sarth.sandymood.interfaces;

public interface DialogCallback {

	void onDialogTimeSet();
	void onDialogDismissed();
}
