package com.sarth.sandymood;

import com.sarth.sandymood.dialogs.MyDialogs;
import com.sarth.sandymood.interfaces.DialogCallback;
import com.sarth.sandymood.utils.MyConstants;
import com.sarth.sandymood.utils.MyPrefs;
import com.sarth.sandymood.utils.MyUtils;

import android.app.Activity;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

public class MainActivity extends Activity implements DialogCallback {

	public static final int MAX_IMAGES = 11;

	private MediaPlayer musicPlayer;
	private int length;
	private int currentImage;
	private long timeToNextImage;
	private ViewSwitcher viewSwitcher;
	private CountDownTimer timerAutoChange;
	private View decorView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MyPrefs.loadSharedPreferences(this);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		viewSwitcher = (ViewSwitcher) findViewById(R.id.switcher);
		viewSwitcher.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				openOptionsMenu();
				return false;
			}
		});
		Toast.makeText(this, R.string.hint_toast, Toast.LENGTH_LONG).show();
		if (MyPrefs.isAutoChangeChecked()) {
			countTimeToNextImage();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		chooseImage(MyPrefs.getImage());
		initMediaPlayer();
		decorView = getWindow().getDecorView();
		MyUtils.hideSystemUI(decorView);
	}

	@Override
	protected void onPause() {
		super.onPause();
		musicPlayer.pause();
		length = musicPlayer.getCurrentPosition();
	}

	/** Timers **/
	private void countTimeToNextImage() {
		timeToNextImage = MyPrefs.getTimeToNextImage();
		timerAutoChange = new CountDownTimer(timeToNextImage, 1000) {

			@Override
			public void onTick(long tick) {
			}

			@Override
			public void onFinish() {
				if (MyPrefs.isAutoChangeChecked()) {
					if (MyPrefs.isRandomImageChecked()) {
						int i = MyUtils.getRandomImage(currentImage);
						chooseImage(i);
					} else {
						setImage(1);
					}
					timerAutoChange.start();
				}
			}
		};
		timerAutoChange.start();
	}

	private void initMediaPlayer() {
		musicPlayer = MediaPlayer.create(this, MyConstants.MUSIC);
		musicPlayer.setLooping(true);
		musicPlayer.seekTo(length);
		musicPlayer.start();
	}

	private void setImage(int direction) {
		currentImage += direction;
		if (currentImage < 0) {
			currentImage = MAX_IMAGES - 1;
		} else if (currentImage > MAX_IMAGES - 1) {
			currentImage = 0;
		}
		chooseImage(currentImage);
	}

	private void chooseImage(int image) {
		// ADD NEW CASES
		ImageView nextImageView = (ImageView) viewSwitcher.getNextView();
		nextImageView.setImageURI(Uri.parse(MyConstants.IMAGE_RESOURCE + "_" + image));
		if (viewSwitcher.getDisplayedChild() == 0) {
			viewSwitcher.showNext();
		} else {
			viewSwitcher.showPrevious();
		}
		currentImage = image;
		MyPrefs.setImage(image);
	}

	/** Options Menu **/
	@Override
	public void openOptionsMenu() {
		super.openOptionsMenu();
		MyUtils.showSystemUI(decorView);
		Configuration config = getResources().getConfiguration();
		if ((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) > Configuration.SCREENLAYOUT_SIZE_LARGE) {
			int originalScreenLayout = config.screenLayout;
			config.screenLayout = Configuration.SCREENLAYOUT_SIZE_LARGE;
			super.openOptionsMenu();
			config.screenLayout = originalScreenLayout;
		} else {
			super.openOptionsMenu();
		}
	}

	@Override
	public void onOptionsMenuClosed(Menu menu) {
		super.onOptionsMenuClosed(menu);
		MyUtils.hideSystemUI(decorView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.my_menu, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.findItem(R.id.auto_change).setChecked(MyPrefs.isAutoChangeChecked());
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.previous:
			setImage(-1);
			break;
		case R.id.next:
			setImage(1);
			break;
		case R.id.auto_change:
			if (MyPrefs.isAutoChangeChecked()) {
				item.setChecked(false);
				MyPrefs.setAutoChangeChecked(false);
			} else {
				MyDialogs.showAutoChangeDialog(this, this);
			}
			break;
		case R.id.report_problem:
			MyDialogs.showReportDialog(this);
			break;
		case R.id.about:
			MyDialogs.showAboutDialog(this);
			break;
		case R.id.exit:
			finish();
			break;
		}
		return false;
	}

	/** DialogCallback Interface **/
	@Override
	public void onDialogTimeSet() {
		invalidateOptionsMenu();
		countTimeToNextImage();
	}

	@Override
	public void onDialogDismissed() {
		// resets the menu item of auto change back to false
		invalidateOptionsMenu();
	}
}